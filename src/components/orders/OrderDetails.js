import React from 'react'
import { Container, Card } from 'react-bootstrap';

const OrderDetails = (props) => {
 let id = props.match.params.id
 return (
   <Container>
    <Card className="my-5">
     <Card.Body>
      <Card.Title>Order title - {id}</Card.Title>

     </Card.Body>
     <Card.Footer>
      Footer Information goes here
     </Card.Footer>
    </Card>
   </Container>
 )
}

export default OrderDetails
