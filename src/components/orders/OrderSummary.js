import React from 'react'
import { Card } from 'react-bootstrap';
import { NavLink } from 'react-router-dom'

const OrderSummary = ({order}) => {
  console.log(order)
 return (
  <Card className="my-4">
     <Card.Body>
      <Card.Title><NavLink to={`/orders/${order.id}`}>{order.name}</NavLink></Card.Title>
      <Card.Text>
        Leorem Ispum
      </Card.Text>
      <footer className="blockquote-footer">
        Someone famous in <cite title="Source Title">Source Title</cite>
      </footer>
     </Card.Body>
    </Card>
 )
}

export default OrderSummary
