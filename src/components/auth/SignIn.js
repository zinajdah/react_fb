import React, { Component } from 'react'
import { Container, Form, Button, Row, Col } from 'react-bootstrap'

class SignIn extends Component {
 state= { }

 _changeHandler = (e) =>{
  let data = {...this.state}
  data[e.target.name] = e.target.value
  this.setState(data)
 }

 _loginHandler = (e) => {
   e.preventDefault()
   alert('submitted!')
 }

 render() {
  console.log(this.state);
  
  return (
   <div>
    <Container>
     <Row className="my-5">
      <Col md={{ span: 4, offset: 4 }}>
      <Form onSubmit={this._loginHandler} method="post">
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control type="email" name="email" onChange={this._changeHandler} placeholder="Enter email" />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" name="password" placeholder="Password" onChange={this._changeHandler} />
        </Form.Group>
        <Form.Group controlId="formBasicChecbox">
          <Form.Check type="checkbox" label="Check me out" />
        </Form.Group>
        <Button variant="primary" type="submit" block>
          Submit
        </Button>
      </Form>
      </Col>
     </Row>
    </Container>
   </div>
  )
 }
}

export default SignIn