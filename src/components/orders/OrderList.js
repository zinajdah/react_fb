import React from 'react'
import OrderSummary from './OrderSummary'

const OrderList = ({orders}) => {

  return (
   <div>  
    {orders && orders.map((order, i)=>
     <OrderSummary key={i} order={order} />
    )
    }
   </div>
  )
}

export default OrderList