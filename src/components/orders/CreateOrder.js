import React, { Component } from 'react'
import { Container, Form, Row, Button, Col} from 'react-bootstrap';
import { connect } from 'react-redux'
import { addOrder } from '../../store/actions/orderAction'

class CreateOrder extends Component {

 state ={
  validated: false,
  order:{}
 }

 _changeHandler = (e) =>{
  let data = {...this.state}
  data.order[e.target.name] = e.target.value
  this.setState(data)
 }

 _createHandler = (event) => {
   
   // alert('submitted!')

   const form = event.currentTarget;
   if (form.checkValidity() === false) {
     alert('not valid')
     event.preventDefault()
     event.stopPropagation()
   }

   event.preventDefault()
   event.stopPropagation()
   let ord = { id:5, name: this.state.order.name, detail : this.state.order.details}
   this.props.add(ord)
   this.setState({ validated: true });
 }

 render() {
  let {validated} = this.state
  console.log(this.state)
  console.log(this.props.add)
  return (
   <Container>
    <Row className="my-3">
    <Col md={{ span: 8, offset: 2 }}>
     <h2>Create New Order</h2>
     </Col>
    </Row>
    <Row className="my-3">
     <Col md={{ span: 8, offset: 2 }}>
     <Form
        noValidate
        validated={validated}
        onSubmit={this._createHandler}
      >
        <Form.Row>
          <Form.Group as={Col} md="12" controlId="validationCustom01">
            <Form.Label>Name</Form.Label>
            <Form.Control
              onChange={this._changeHandler}
              required
              name="name"
              type="text"
              placeholder="First name"
              // defaultValue="Mark"
            />
            <Form.Control.Feedback type="invalid">
              Please provide a valid Firstname.
            </Form.Control.Feedback>
            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
          </Form.Group>
        </Form.Row>
        <Form.Row>
          <Form.Group as={Col} md="12" controlId="validationCustom03">
            <Form.Label>Details</Form.Label>
            <Form.Control as="textarea" rows="3" placeholder="City" required onChange={this._changeHandler} name="details" />
            <Form.Control.Feedback type="invalid">
              Please provide a valid detail.
            </Form.Control.Feedback>
          </Form.Group>
        </Form.Row>
        <Button type="submit" block>Submit form</Button>
      </Form>
     </Col>
    </Row>
   </Container>
  )
 }
}

const mapDispatchToProps = (dispatch) =>{
  return {
    add : (order) => dispatch(addOrder(order))
  }
}

export default connect(null, mapDispatchToProps)(CreateOrder)