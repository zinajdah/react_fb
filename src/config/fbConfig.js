import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';


// Initialize Firebase
const firebaseConfig = {
  apiKey: "AIzaSyAjycQKV6iOxoeHoCdUAQD9pX_j1qYmUkw",
  authDomain: "reactfb-22371.firebaseapp.com",
  databaseURL: "https://reactfb-22371.firebaseio.com",
  projectId: "reactfb-22371",
  storageBucket: "reactfb-22371.appspot.com",
  messagingSenderId: "880202351625",
  appId: "1:880202351625:web:5878bb1a02efd9cc"
 };
firebase.initializeApp(firebaseConfig);
// firebase.firestore(); // We don't need this anymore because we create firestore in index.js

export default firebase;
