import React from 'react'
import {NavLink} from 'react-router-dom'
import  {Navbar, Nav, NavDropdown} from 'react-bootstrap'

const Navigation = (props) => {
 // if()
 return (
  <Navbar bg="dark" variant="dark">
  <Navbar.Brand href="/">Navbar</Navbar.Brand>
   <Navbar.Toggle aria-controls="basic-navbar-nav" />
   <Navbar.Collapse id="basic-navbar-nav">
     <Nav className="mr-auto justify-content-end">
       <NavLink className="nav-link" to="/dashboard">Dashboard</NavLink>
        <NavDropdown title="Orders" id="collasible-nav-dropdown">
          <NavLink className="dropdown-item" to="/orders">All Orders</NavLink>
          <NavLink className="dropdown-item" to="/orders/create">Create Orders</NavLink>
        </NavDropdown>
       <NavLink className="nav-link" to="/auth/signin">Signin</NavLink>
       <NavLink className="nav-link" to="/auth/signup">Signup</NavLink>
       <NavLink className="nav-link" to="/auth/profile">MM</NavLink>
     </Nav>
   </Navbar.Collapse>
  </Navbar>
 )
}

export default Navigation
