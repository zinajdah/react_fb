import React, { Component } from 'react'
import { Container, Row, Col } from 'react-bootstrap';
import OrderList from '../orders/OrderList';
import Notification from '../layouts/Notification';
import {compose} from 'redux' // get higher order component form redux to connect to collection
import {firestoreConnect } from 'react-redux-firebase'

import {connect} from 'react-redux'

class Dashboard extends Component {
 render() {
  let {orders} = this.props
  return (
   <div>
    <Container>
     <Row>
       <Col xs={12} md={6}>
         <OrderList orders={orders}/>
       </Col>
       <Col xs={12} md={6}>
        <Notification />
       </Col>
     </Row>
    </Container>
   </div>
  )
 }
}

const mapStateToProps = (state, props)=>{
  console.log(state)
  return {
   orders : state.order.orders
  }
 }

export default compose(
  connect(mapStateToProps),
  firestoreConnect([
    { collection : 'orders'} //listens for changes in firebase firestore
  ])
  )(Dashboard)

// export default connect(mapStateToProps)(Dashboard)