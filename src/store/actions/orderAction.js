export const FETCH_ORDERS = 'FETCH_ORDERS'
export const GET_ORDER = 'GET_ORDER'
export const ADD_ORDER = 'ADD_ORDER'

//Errors
export const ADD_ORDER_ERR = 'ADD_ORDER_ERR'


export const fetchOrders = () =>{
 return {
  
 }
}

export const addOrder = (order) => (dispatch, getState, {getFirestore }) =>{

 const firestore = getFirestore()
 // const {profile} = getState().firebase

 firestore.collection('orders').add({
  ...order,
  firstName: 'didi',
  lastName: 'lowkey',
  orderId: 1234,
  createdAt: new Date()
 }).then(()=>{
  //call async to db
  dispatch({
    type: ADD_ORDER,
    payload : order
   })
 }).catch(e =>{
  dispatch({
   type: 'ADD_ORDER_ERR',
   payload : e
  })
 })

 
  // return {
  //  type: ADD_ORDER,
  //  payload : order
  // }
}