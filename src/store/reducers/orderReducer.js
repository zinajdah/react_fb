import { ADD_ORDER, ADD_ORDER_ERR } from '../actions/orderAction'

const initState = {
 orders : [
  { id: 1, name: 'books', details : 'books for sale'},
  { id: 2, name: 'books2', details : 'books for sale'},
  { id: 3, name: 'books3', details: 'books for sale'}
 ]
}

const orderReducer = (state = initState, action) =>{
 switch(action.type){
  case ADD_ORDER:
   console.log(action.payload)
   return action.payload
  case ADD_ORDER_ERR:
    console.log(action.payload)
    return action.payload
  default:
   return state
 }

 // return state
}

export default orderReducer