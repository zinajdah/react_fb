import React, { Component } from 'react'
import { Container, Form, Row, Button, Col } from 'react-bootstrap';

class Signup extends Component {

 state ={validated: false}

 _changeHandler = (e) =>{
  let data = {...this.state}
  data[e.target.name] = e.target.value
  this.setState(data)
 }

 _registerHandler = (event) => {
   
   // alert('submitted!')

   const form = event.currentTarget;
   if (form.checkValidity() === false) {
     alert('not valid')
     event.preventDefault()
     event.stopPropagation()
   }

   event.preventDefault()
   event.stopPropagation()
   this.setState({ validated: true });
 }

 render() {
  let {validated} = this.state
  console.log(this.state)
  return (
   <Container>
    <Row className="my-5">
     <Col md={{ span: 8, offset: 2 }}>
     <Form
        noValidate
        validated={validated}
        onSubmit={this._registerHandler}
      >
        <Form.Row>
          <Form.Group as={Col} md="4" controlId="validationCustom01">
            <Form.Label>First name</Form.Label>
            <Form.Control
              onChange={this._changeHandler}
              required
              name="firstname"
              type="text"
              placeholder="First name"
              // defaultValue="Mark"
            />
            <Form.Control.Feedback type="invalid">
              Please provide a valid Firstname.
            </Form.Control.Feedback>
            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
          </Form.Group>
          <Form.Group as={Col} md="4" controlId="validationCustom02">
            <Form.Label>Last name</Form.Label>
            <Form.Control
              onChange={this._changeHandler}
              required
              type="text"
              name="lastname"
              placeholder="Last name"
              defaultValue="Otto"
            />
            
            <Form.Control.Feedback type="invalid">
              Please provide a valid Firstname.
            </Form.Control.Feedback>
            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
          </Form.Group>
          <Form.Group as={Col} md="4" controlId="validationCustomEmail">
            <Form.Label>Email</Form.Label>
              <Form.Control
                type="text"
                onChange={this._changeHandler}
                name="email"
                placeholder="Email"
                required
              />
              <Form.Control.Feedback type="invalid">
                Please choose a email.
              </Form.Control.Feedback>
          </Form.Group>
        </Form.Row>
        <Form.Row>
          <Form.Group as={Col} md="6" controlId="validationCustom03">
            <Form.Label>City</Form.Label>
            <Form.Control type="text" placeholder="City" required onChange={this._changeHandler} name="city" />
            <Form.Control.Feedback type="invalid">
              Please provide a valid city.
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group as={Col} md="6" controlId="validationCustom04">
            <Form.Label>State</Form.Label>
            <Form.Control type="text" placeholder="State" required onChange={this._changeHandler} name="state"/>
            <Form.Control.Feedback type="invalid">
              Please provide a valid state.
            </Form.Control.Feedback>
          </Form.Group>
        </Form.Row>
        <Form.Group>
          <Form.Check
            required
            label="Agree to terms and conditions"
            feedback="You must agree before submitting."
          />
        </Form.Group>
        <Button type="submit" block>Submit form</Button>
      </Form>
     </Col>
    </Row>
   </Container>
  )
 }
}

export default Signup