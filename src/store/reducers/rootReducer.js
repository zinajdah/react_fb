 import { combineReducers } from 'redux'
 import { firestoreReducer } from 'redux-firestore'
 import { firebaseReducer } from 'react-redux-firebase'
 

 import authReducer from './authReducer'
 import orderReducer from './orderReducer'


 let rootReducer = combineReducers({ 
    auth: authReducer, 
    order: orderReducer,
    firestore: firestoreReducer, //syncs from firestore
    firebase: firebaseReducer,
   })

 export default rootReducer 