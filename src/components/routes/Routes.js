import Dashboard from '../dashboard/Dashboard' 
import OrderList from '../orders/OrderList' 
import SignIn from '../auth/SignIn'
import Signup from '../auth/Signup'
import OrderDetails from '../orders/OrderDetails' 
import CreateOrder from '../orders/CreateOrder' 

const routes = [
 {
  component: Dashboard,
  path: "/",
  exact: true
 },
 {
  component: Dashboard,
  path: "/dashboard",
  exact: false
 },
 {
  component: OrderList,
  path: "/orders",
  exact: true
 },
 {
  component: CreateOrder,
  path: "/orders/create",
  exact: true
 },
 {
  component: OrderDetails,
  path: "/orders/:id",
  exact: false
 },
 {
  component: SignIn,
  path: "/auth/signin",
  exact: true
 },
 {
  component: Signup,
  path: "/auth/signup",
  exact: true
 }
]


export default routes