import React, {Component} from 'react';
// import { Button, Alert, Navbar } from 'react-bootstrap';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

import routes from './components/routes/Routes'

// import { compose } from 'redux';
// import { connect } from 'react-redux';
// import { firebaseConnect } from 'react-redux-firebase';

import Navigation from './components/layouts/Navigation'

class App extends Component {
  _getRoutes = () =>{
    return (
      routes.map((rout, i) =>{

      
      if(!rout.exact){
        return ( <Route key={i} path={rout.path} component={rout.component} />)
      }else{
        return ( <Route exact key={i} path={rout.path} component={rout.component} />)
      }
      

      })
    )
  } 

  render(){
    return (
     <Router>
       {/* Navigation bar */}
       <Navigation />

      {/* Routes */}
      <Switch>
        {this._getRoutes() }
      </Switch>

     </Router>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.firebase.auth,
});

// export default compose(
//   firebaseConnect(),
//   connect(mapStateToProps),
// )(App);

export default App;
